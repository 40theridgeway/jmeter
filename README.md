JMeter distrib and JMeter docker image (simple and with Chrome for WebDriver) for automated loadtesting


**/jmeter/jmeter-base** - docker image for JMeter

**/jmeter/jmeter-base-chrome** - docker image for JMeter with Chrome

*Images*

Simple docker image --- *alpine* -> jmeter-base!

Chrome docker image --- *alpine* -> chrome + jmeter-base-chrome!